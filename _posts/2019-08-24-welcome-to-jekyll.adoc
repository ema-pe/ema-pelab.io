---
tags:
  - food
  - trip
categories:
  - Misc
  - Idea
---
= Welcome to Jekyll
:page-liquid:
:page-layout: post
:page-date: 2019-08-24 13:10:32 +0200

You’ll find this post in your `_posts` directory. Go ahead and edit it and
re-build the site to see your changes. You can rebuild the site in many
different ways, but the most common way is to run `jekyll serve`, which launches
a web server and auto-regenerates your site when a file is updated.

To add new posts, simply add a file in the `_posts` directory that follows the
convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front
matter. Take a look at the source for this post to get an idea about how it
works.

Jekyll also offers powerful support for code snippets:

++++
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}
++++

Check out the https://jekyllrb.com/docs/home[Jekyll docs] for more info on how
to get the most out of Jekyll. File all bugs/feature requests at
https://github.com/jekyll/jekyll[Jekyll’s GitHub repo]. If you have questions,
you can ask them on https://talk.jekyllrb.com/[Jekyll Talk].

NOTE: this is a note!

TIP: this is a tip!

IMPORTANT: this is important!

CAUTION: this is caution!

WARNING: this is a warning!
